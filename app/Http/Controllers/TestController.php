<?php

namespace App\Http\Controllers;

use EtherpadLite\Exception\UnsupportedMethodException;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Http\Request;
use Cookie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class TestController extends Controller
{
    public function index()
    {
        $client = new \EtherpadLite\Client('7a00afb9890f73bdc4c0fa176d186aeedec496ed11a1792d33a73af816cca886');
        $response = $client->checkToken();
        $pad_id=$client->generatePadID();
        $user_id=Auth::user()->name;
        $authorResponse=$client->__call('createAuthorIfNotExistsFor',[Auth::id(), Auth::user()->name]);
        $file=file_get_contents(public_path().'\test2.html');

        $groupResponse=$client->__call('createGroupIfNotExistsFor',[Auth::id(), Auth::user()->name]);
        $groupPadResponse=$client->__call('createGroupPad',[$groupResponse->getData()['groupID'], 'test_7t884yw', 'Dummy Text']);
        $groupSessionResponse=$client->__call('createSession',[$groupResponse->getData()['groupID'], $authorResponse->getData()['authorID'], '2212201246']);
        $sessionID = $groupSessionResponse->getData()['sessionID'];
        $apiKey='7a00afb9890f73bdc4c0fa176d186aeedec496ed11a1792d33a73af816cca886';
//
        dd('here');
         $pad2=$client->__call('createPad',[$pad_id, 'Test Pad Text']);
        dd($client->__call('setHTML',[$pad_id, $file]));
//
//        $header = [
//            'set-cookie' => ['Cookie' => 'sessionID='.$sessionID],
//            'form_params' => [
//                'file' =>  $file,
//            ]
//        ];
//        $response = Http::post('http://localhost:9001/p/'. $pad_id .'/import');
////        $response->header($header);
//        $response->headers();
//        $response = $this->httpClient->request('GET', $url, $request_options);

//        $cookieJar = CookieJar::fromArray([
//            'sessionID' => $sessionID
//        ]);

//         $client = new Client();
//        $res = $client->request('POST', 'http://localhost:9001/p/'. $pad_id .'/import', $request_options);
//        $res = $client->post(, ['file' =>  $file]);
//        dd($response->headers()); // 200

//        $cookie = cookie('name', 'value', $sessionID);
//        $response->withCookie(cookie()->forever('name', 'value'));


// if you don't use http://localhost:9001
//$client = new \EtherpadLite\Client($apikey, 'http://example.com:9001');
//       $response1=$client->__call('createGroup');
//        $groupID= $response1->getData()['groupID'];
//
//        $client->__call('createGroupPad',[$groupID, 'test_pad', 'text']);

        /** @var $response \EtherpadLite\Response */
//        try {
//           // dd("herehere");
//           $client->__call('createPad', $pad_id, 'Hello World');
//        } catch (UnsupportedMethodException $e) {
//            dd("exception", $e);
//        }
//        $response = $client->checkToken();
      //  dd($pad2);
//        $cookie = Cookie::make('sessionID', $sessionID);
//        return redirect('http://localhost:9001/p/'.$groupPadResponse->getData()['padID'].'/export/html', 302)->withCookie($cookie);;
            return view('welcome', ['key' => $pad_id])
                ->with('sessionID',$sessionID)->with('padID', $groupPadResponse->getData()['padID'])->with('api', $apiKey)->with('pad2',$pad_id);
      //  dd($response, $response->getCode(), $response->getMessage(), $response->getData());

    }
}
